enum bool testAdd();

enum bool testSubtract();

enum bool testMultiply();

enum bool testDivide();

enum bool testSquare();

enum bool testCube();

enum bool testRaise();

enum bool testSquareRoot();

enum bool testCubicRoot();

enum bool testNthRoot();

enum bool testBase10Logarithm();

enum bool testNaturalLogarithm();

enum bool testEulerConstant();

enum bool testBaseNLogarithm();

enum bool testCosine();

enum bool testSine();

enum bool testTangent();

enum bool testPi();

enum bool testDegreesToRadians();

enum bool testArcCosine();

enum bool testArcSine();

enum bool testArcTangent();

enum bool testChangeSign();

enum bool testAbsoluteValue();

enum bool testInvert();

enum bool testPercentage();

enum bool testFactorial();

enum bool testPowerOf10();

enum bool testHyperbolicCosine();

enum bool testHyperbolicSine();

enum bool testHyperbolicTangent();

enum bool testClosestInteger();

enum bool testMax();

void runAllTests();
