#include "ITAndroidsCalculator.h"
#include "tests.h"
enum bool { false = 0, true = 1};

/// FUNCTION 1
enum bool testAdd(){
    if((add(21.0, 21.0) - 42.0) <= 0.00001)
        return true;

    printf("Add function failed.\n");
    return false;
}

/// FUNCTION 2
enum bool testSubtract(){
    if((subtract(24.0, 21.0) - 3.0) <= 0.00001)
        return true;

    printf("Subtract function failed.\n");
    return false;
}

/// FUNCTION 3
enum bool testMultiply(){
    if((multiply(21.0, 21.0) - 441.0) <= 0.00001)
        return true;

    printf("Multiply function failed.\n");
    return false;
}

/// FUNCTION 4
enum bool testDivide(){
    if((divide(21.0, 2.0) - 10.5) <= 0.00001)
        return true;

    printf("Divide function failed.\n");
    return false;
}

/// FUNCTION 5
enum bool testSquare(){
    if((square(21.0) - 441.0) <= 0.00001)
        return true;

    printf("Square function failed.\n");
    return false;
}

/// FUNCTION 6
enum bool testCube(){
    if((cube(21.0) - 9261.0) <= 0.00001)
        return true;

    printf("Cube function failed.\n");
    return false;
}

/// FUNCTION 7
enum bool testRaise(){
    if((raise(21.0, 4.0) - 194481.0) <= 0.00001)
        return true;

    printf("Raise function failed.\n");
    return false;
}

/// FUNCTION 8
enum bool testSquareRoot(){
    if((squareRoot(121.0) - 11.0) <= 0.00001)
        return true;

    printf("Square root function failed.\n");
    return false;
}

/// FUNCTION 9
enum bool testCubicRoot(){
    if((cubicRoot(9261.0) - 21.0) <= 0.00001)
        return true;

    printf("Cubic root function failed.\n");
    return false;
}

/// FUNCTION 10
enum bool testNthRoot(){
    if((nthRoot(194481.0, 4) - 21.0) <= 0.00001)
        return true;

    printf("Nth root function failed.\n");
    return false;
}

/// FUNCTION 11
enum bool testBase10Logarithm(){
    if((base10Logarithm(1000.0) - 3.0) <= 0.00001)
        return true;

    printf("Base-10 logarithm function failed.\n");
    return false;
}

/// FUNCTION 12
enum bool testNaturalLogarithm(){
    if((naturalLogarithm(2.718281828459045235360287) - 1.0) <= 0.00001)
        return true;

    printf("Natural logarithm function failed.\n");
    return false;
}

/// FUNCTION 13
enum bool testEulerConstant(){
    if((eulerConstant(2) - 2.72) <= 0.00001)
        return true;

    printf("Euler constant function failed.\n");
    return false;
}

/// FUNCTION 14
enum bool testBaseNLogarithm(){
    if((baseNLogarithm(8.0, 2) - 3.0) <= 0.00001)
        return true;

    printf("Base-n logarithm function failed.\n");
    return false;
}

/// FUNCTION 15
enum bool testCosine(){
    if((cosine(1.047197551) - 0.5) <= 0.00001)
        return true;

    printf("Cosine function failed.\n");
    return false;
}

/// FUNCTION 16
enum bool testSine(){
    if((sine(0.523598776) - 0.5) <= 0.00001)
        return true;

    printf("Sine function failed.\n");
    return false;
}

/// FUNCTION 17
enum bool testTangent(){
    if((tangent(0.785398163) - 1.0) <= 0.00001)
        return true;

    printf("Tangent function failed.\n");
    return false;
}

/// FUNCTION 18
enum bool testPi(){
    if((pi(2) - 3.14) <= 0.00001)
        return true;

    printf("Pi function failed.\n");
    return false;
}

/// FUNCTION 19
enum bool testDegreesToRadians(){
    if((degreesToRadians(180) - 3.14159265359) <= 0.00001)
        return true;

    printf("Degrees to radians function failed.\n");
    return false;
}
/// FUNCTION 20
enum bool testArcCosine(){
    if((arcCosine(0.5) - 1.047197551) <= 0.00001)
        return true;

    printf("Arc cosine function failed.\n");
    return false;
}

/// FUNCTION 21
enum bool testArcSine(){
    if((arcSine(0.5) - 0.523598776) <= 0.00001)
        return true;

    printf("Arc sine function failed.\n");
    return false;
}

/// FUNCTION 22
enum bool testArcTangent(){
    if((arcTangent(1.0) - 0.785398163) <= 0.00001)
        return true;

    printf("Arc tangent function failed.\n");
    return false;
}

/// FUNCTION 23
enum bool testChangeSign(){
    if((changeSign(-21.0) - 21.0) <= 0.00001)
        return true;

    printf("Change sign function failed.\n");
    return false;
}

/// FUNCTION 24
enum bool testAbsoluteValue(){
    if((absoluteValue(-21.0) - 21.0) <= 0.00001)
        return true;

    printf("Absolute value function failed.\n");
    return false;
}

/// FUNCTION 25
enum bool testInvert(){
    if((invert(4.0) - 0.25) <= 0.00001)
        return true;

    printf("Invert function failed.\n");
    return false;
}

/// FUNCTION 26
enum bool testPercentage(){
    if((percentage(21.0) - 0.21) <= 0.00001)
        return true;

    printf("Factorial function failed.\n");
    return false;
}

/// FUNCTION 27
enum bool testFactorial(){
    if((factorial(4.0) - 24.0) <= 0.00001)
        return true;

    printf("Factorial function failed.\n");
    return false;
}

/// FUNCTION 28
enum bool testPowerOf10(){
    if(powerOf10(4) == 10000.0)
        return true;

    printf("Power of 10 function failed.\n");
    return false;
}

/// FUNCTION 29
enum bool testHyperbolicCosine(){
    if((hyperbolicCosine(0.0) - 1.0) <= 0.00001)
        return true;

    printf("Hyperbolic cosine function failed.\n");
    return false;
}

/// FUNCTION 30
enum bool testHyperbolicSine(){
    if((hyperbolicSine(0.0) - 0.0) <= 0.00001)
        return true;

    printf("Hyperbolic sine function failed.\n");
    return false;
}

/// FUNCTION 31
enum bool testHyperbolicTangent(){
    if((hyperbolicTangent(0.0) - 0.0) <= 0.00001)
        return true;

    printf("Hyperbolic sine function failed.\n");
    return false;
}

/// FUNCTION 32
enum bool testClosestInteger(){
    if(closestInteger(21.4) == 21)
        return true;

    printf("Closest integer function failed.\n");
    return false;
}

/// FUNCTION 33
enum bool testMax(){
    if((max(21.0, 20.0) - 21.0) <= 0.00001)
        return true;

    printf("Max function failed.\n");
    return false;
}


void runAllTests(){
    if( testAdd() && testSubtract() && testMultiply() && testDivide() && testSquare() && testCube() &&
        testRaise() && testSquareRoot() && testCubicRoot() && testNthRoot() && testBase10Logarithm() &&
        testNaturalLogarithm() && testEulerConstant() && testBaseNLogarithm() && testCosine() &&
        testSine() && testTangent() && testPi() && testDegreesToRadians() && testArcCosine() &&
        testArcSine() && testArcTangent() && testChangeSign() && testAbsoluteValue() && testInvert() &&
        testPercentage() && testFactorial() && testPowerOf10() && testHyperbolicCosine() &&
        testHyperbolicSine() && testHyperbolicTangent() && testClosestInteger() && testMax())
        printf("All tests passed!\n");

}
