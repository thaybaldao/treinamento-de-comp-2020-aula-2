#include "ITAndroidsCalculator.h"
#include <stdio.h>
#include <math.h>
/// FUNCTION 1
/*******************************************************/
/** DESCRIPTION: this function should add two numbers **/
/** ARGUMENTS: the two numbers that should be added   **/
/** RETURN: the sum of the arguments                  **/
/*******************************************************/
double add(double num1, double num2){
    return num1 + num2;
}

/// FUNCTION 2
/********************************************************************************************************************/
/** DESCRIPTION: this function should subtract two numbers                                                         **/
/** ARGUMENTS: the two numbers that should be subtracted, num2 is the number that should be subtracted from num1   **/
/** RETURN: the subtraction of the arguments                                                                       **/
/********************************************************************************************************************/
double subtract(double num1, double num2){
    return num1 - num2;
}

/// FUNCTION 3
/*************************************************************/
/** DESCRIPTION: this function should multiply two numbers  **/
/** ARGUMENTS: the two numbers that should be multiplied    **/
/** RETURN: the multiplication of the arguments             **/
/*************************************************************/
double multiply(double num1, double num2){
    return num1*num2;
}

/// FUNCTION 4
/*****************************************************************************************************/
/** DESCRIPTION: this function should divide two numbers                                            **/
/** ARGUMENTS: the two numbers that should be divided, num1 is the dividend and num2 is the divisor **/
/** RETURN: the division of the arguments                                                           **/
/*****************************************************************************************************/
double divide(double num1, double num2){
    return num1/num2;
}

/// FUNCTION 5
/********************************************************/
/** DESCRIPTION: this function should square a number  **/
/** ARGUMENTS: the number that should be squared       **/
/** RETURN: the square of the argument                 **/
/********************************************************/
double square(double num){
    return num*num;
}

/// FUNCTION 6
/******************************************************/
/** DESCRIPTION: this function should cube a number  **/
/** ARGUMENTS: the number that should be cubed       **/
/** RETURN: the cube of the argument                 **/
/******************************************************/
double cube(double num){
    return num*num*num;
}

/// FUNCTION 7
/************************************************************************************/
/** DESCRIPTION: this function should raise a specific number to a specific power  **/
/** ARGUMENTS: the numbers corresponding to the base and the power                 **/
/** RETURN: the result of the raise                                                **/
/************************************************************************************/
double raise(double base, double power){
    return pow(base,power);
}

/// FUNCTION 8
/************************************************************************/
/** DESCRIPTION: this function should get the square root of a number  **/
/** ARGUMENTS: the number which you should get the square root of      **/
/** RETURN: the square root of the argument                            **/
/************************************************************************/
double squareRoot(double num){
    return sqrt(num);
}

/// FUNCTION 9
/***********************************************************************/
/** DESCRIPTION: this function should get the cubic root of a number  **/
/** ARGUMENTS: the number which you should get the cubic root of      **/
/** RETURN: the cubic root of the argument                            **/
/***********************************************************************/
double cubicRoot(double num){
    return pow(num, 0.3333333);
}

/// FUNCTION 10
/***********************************************************************/
/** DESCRIPTION: this function should get the nth root of a number    **/
/** ARGUMENTS: the number which you should get the nth root of and n  **/
/** RETURN: the nth root of the argument                              **/
/***********************************************************************/
double nthRoot(double num, double n){
    return pow(num, 1.0/n);
}

/// FUNCTION 11
/*****************************************************************************/
/** DESCRIPTION: this function should get the base-10 logarithm of a number **/
/** ARGUMENTS: the number which you should get the base-10 logarithm of     **/
/** RETURN: the base-10 logarithm of the argument                           **/
/*****************************************************************************/
double base10Logarithm(double num){
    return log10(num);
}

/// FUNCTION 12
/*****************************************************************************/
/** DESCRIPTION: this function should get the natural logarithm of a number **/
/** ARGUMENTS: the number which you should get the natural logarithm of     **/
/** RETURN: the natural logarithm of the argument                           **/
/*****************************************************************************/
double naturalLogarithm(double num){
    return log(num);
}

/// FUNCTION 13
/************************************************************************************************************/
/** DESCRIPTION: this function should return the euler constant with an specific ammount of decimal places **/
/** ARGUMENTS: the specific ammount of decimal places                                                      **/
/** RETURN: the euler constant with the specific ammount of decimal places                                 **/
/************************************************************************************************************/
double eulerConstant(int decimalPlaces){
    return M_E-(M_E*pow(10,decimalPlaces) - floor(M_E*pow(10,decimalPlaces)))/pow(10,decimalPlaces);
}

/// FUNCTION 14
/******************************************************************************/
/** DESCRIPTION: this function should get the base-n logarithm of a number   **/
/** ARGUMENTS: the number which you should get the base-n logarithm of and n **/
/** RETURN: the base-n logarithm of the argument                             **/
/******************************************************************************/
double baseNLogarithm(double num, int n){
    return log10(num)/log10(n);
}

/// FUNCTION 15
/***************************************************************************/
/** DESCRIPTION: this function should get the cosine of a number          **/
/** ARGUMENTS: the number which you should get the cosine of (in radians) **/
/** RETURN: the cosine of the argument                                    **/
/***************************************************************************/
double cosine(double num){
    return cos(num);
}

/// FUNCTION 16
/*************************************************************************/
/** DESCRIPTION: this function should get the sine of a number          **/
/** ARGUMENTS: the number which you should get the sine of (in radians) **/
/** RETURN: the sine of the argument                                    **/
/*************************************************************************/
double sine(double num){
    return sin(num);
}

/// FUNCTION 17
/*******************************************************************************/
/** DESCRIPTION: this function should get the tangent of a number             **/
/** ARGUMENTS: the number which you should get the tangent of (in radians)    **/
/** RETURN: the tangent of the argument                                       **/
/*******************************************************************************/
double tangent(double num){
    return (tan(num));
}

/// FUNCTION 18
/*******************************************************************************************************/
/** DESCRIPTION: this function should return the pi number with an specific ammount of decimal places **/
/** ARGUMENTS: the specific ammount of decimal places                                                 **/
/** RETURN: the pi number with the specific ammount of decimal places                                 **/
/*******************************************************************************************************/
double pi(int decimalPlaces){
    return M_PI - (M_PI*pow(10,decimalPlaces) - floor(M_PI*pow(10,decimalPlaces)))/pow(10,decimalPlaces);
}

/// FUNCTION 19
/*****************************************************************************************/
/** DESCRIPTION: this function should get an angle in deegres and convert it to radians **/
/** ARGUMENTS: the angle in degrees                                                     **/
/** RETURN: the corresponding angle in radians                                          **/
/*****************************************************************************************/
double degreesToRadians(double num){
    return (3.141592*num)/180;
}
/// FUNCTION 20
/**********************************************************************/
/** DESCRIPTION: this function should get the arc cosine of a number **/
/** ARGUMENTS: the number which you should get the arc cosine of     **/
/** RETURN: the arc cosine of the argument                           **/
/**********************************************************************/
double arcCosine(double num){
    return acos (num);
}

/// FUNCTION 21
/********************************************************************/
/** DESCRIPTION: this function should get the arc sine of a number **/
/** ARGUMENTS: the number which you should get the arc sine of     **/
/** RETURN: the arc sine of the argument                           **/
/********************************************************************/
double arcSine(double num){
    return asin (num);
}

/// FUNCTION 22
/***********************************************************************/
/** DESCRIPTION: this function should get the arc tangent of a number **/
/** ARGUMENTS: the number which you should get the arc tangent of     **/
/** RETURN: the arc tangent of the argument                           **/
/***********************************************************************/
double arcTangent(double num){
    return atan(num);
}

/// FUNCTION 23
/************************************************************************/
/** DESCRIPTION: this function should get a number and change its sign **/
/** ARGUMENTS: the number which you should change sign                 **/
/** RETURN: the argument with sign changed                             **/
/************************************************************************/
double changeSign(double num){
    num=-num;
    return num;
}

/// FUNCTION 24
/********************************************************************************/
/** DESCRIPTION: this function should calculate the absolute value of a number **/
/** ARGUMENTS: the number which you should calculate the absolute value of     **/
/** RETURN: the absolute value of the argument                                 **/
/********************************************************************************/
double absoluteValue(double num){
    return fabs(num);
}

/// FUNCTION 25
/***************************************************************************/
/** DESCRIPTION: this function should get a number and return its inverse **/
/** ARGUMENTS: the number which you should invert                         **/
/** RETURN: the rgument inverted                                          **/
/***************************************************************************/
double invert(double num){
    return 1/num;
}

/// FUNCTION 26
/***************************************************************************/
/** DESCRIPTION: this function should calculate the percentage of a number **/
/** ARGUMENTS: the number which you should calculate the percentage of     **/
/** RETURN: the percentage of the argument                                 **/
/***************************************************************************/
double percentage(double num){
    return num/100;
}

/// FUNCTION 27
/***************************************************************************/
/** DESCRIPTION: this function should calculate the factorial of a number **/
/** ARGUMENTS: the number which you should calculate the factorial of     **/
/** RETURN: the factorial of the argument                                 **/
/***************************************************************************/
int factorial(int num){
    int i, fact=1;
    for(i=1;i<=num;i++){
    	fact=fact*i;
    }

    return fact;

}

/// FUNCTION 28
/****************************************************************************/
/** DESCRIPTION: this function should calculate the percentage of a number **/
/** ARGUMENTS: the number which you should calculate the percentage of     **/
/** RETURN: the percentage of the argument                                 **/
/****************************************************************************/
int powerOf10(double num){
return pow (10.0,num);
}


/// FUNCTION 29
/*****************************************************************************/
/** DESCRIPTION: this function should get the hyperbolic cosine of a number **/
/** ARGUMENTS: the number which you should get the hyperbolic cosine of     **/
/** RETURN: the hyperbolic cosine of the argument                           **/
/*****************************************************************************/
double hyperbolicCosine(double num){
    return cosh(num);
}

/// FUNCTION 30
/***************************************************************************/
/** DESCRIPTION: this function should get the hyperbolic sine of a number **/
/** ARGUMENTS: the number which you should get the hyperbolic sine of     **/
/** RETURN: the hyperbolic sine of the argument                           **/
/***************************************************************************/
double hyperbolicSine(double num){
    return 0.0;
}

/// FUNCTION 31
/******************************************************************************/
/** DESCRIPTION: this function should get the hyperbolic tangent of a number **/
/** ARGUMENTS: the number which you should get the hyperbolic tangent of     **/
/** RETURN: the hyperbolic tangent of the argument                           **/
/******************************************************************************/
double hyperbolicTangent(double num){
    return 0.0;
}

/// FUNCTION 32
/******************************************************************************/
/** DESCRIPTION: this function should get the closest integer to this number **/
/** ARGUMENTS: the number which you should get the closest integer of        **/
/** RETURN: the closest integer of the argument                              **/
/******************************************************************************/
int closestInteger(double num){
    return 21.0;
}

/// FUNCTION 33
/******************************************************************************/
/** DESCRIPTION: this function should return the biggest between two numbers **/
/** ARGUMENTS: the numbers which you are comparing                           **/
/** RETURN: the biggest argument                                             **/
/******************************************************************************/
double max(double num1, double num2){
    return 21.0;
}
